## 文章目录调整

1. **`SpringBoot1.x`目录下包含了SpringBoot1.5.x版本章节源码。**
2. **`SpringBoot2.x`目录下包含了SpringBoot2.1.x版本章节源码。**

恒宇少年最近在筹划`ApiBoot`接口服务框架以及`恒宇少年的知识库`文章分享的小程序，更新`SpringBoot2.x`版本文章比较少，在往后的时间里，会陆续更新相关文章。

关注`ApiBoot`框架的途径：

- [GitHub](https://github.com/hengboy/api-boot)
- [码云](https://gitee.com/hengboy/api-boot)

### Spring2.1.x更新计划

恒宇少年规划继续更新`SpringBoot 2.1.x`版本的相关内容，提供更多常用框架的集成解决方案，下面是大致的规划相关的内容：

- Spring Security
- Oauth2
- Mybatis Enhance
- Druid
- HikariCP
- 分布式锁
- Apollo 分布式配置中心
- Nacos 分布式配置中心
- Leaf 分布式唯一主键
- Jenkins 持续集成工具
- Nginx + Jar负载均衡部署

### SpringBoot版本章节源码

章节源码目录整理后，点击下面链接可以进入不同版本的源码目录。

|序号		| SpringBoot版本     			|描述	 |
|:-----:	| :---------------------------------------------------------------------|:-----------|
|001		| [SpringBoot 1.x版本章节源码](https://gitee.com/hengboy/spring-boot-chapter/tree/master/SpringBoot1.x)	|SpringBoot1.x版本文章对应源码|
|002		| [SpringBoot2.1.x版本章节源码](https://gitee.com/hengboy/spring-boot-chapter/tree/master/SpringBoot2.x)	|新：SpringBoot2.1.x版本文章对应源码|

### ApiBoot + SpringBoot = Partner ！！！

`ApiBoot`是一款基于`SpringBoot1.x`、`SpringBoot2.x`的接口服务集成基础框架，内部提供了框架的封装集成、使用扩展、自动化完成配置，让接口开发者可以选着性完成开箱即用，不再为搭建接口框架而犯愁，从而极大的提高开发效率。

`ApiBoot`接口服务组件集成落地解决方案相关的使用文章也会有对应的更新，主要包含的组件如下所示：

- [ApiBoot Security Oauth](https://github.com/hengboy/api-boot/wiki/ApiBoot-Security-Oauth)
- [ApiBoot Swagger](https://github.com/hengboy/api-boot/wiki/ApiBoot-Swagger)
- [ApiBoot Http Converter](https://github.com/hengboy/api-boot/wiki/ApiBoot-Http-Converter)
- [ApiBoot Alibaba OSS](https://github.com/hengboy/api-boot/wiki/ApiBoot-Alibaba-Oss)
- [ApiBoot Alibaba SMS](https://github.com/hengboy/api-boot/wiki/ApiBoot-Alibaba-Sms)
- [ApiBoot Quartz](https://github.com/hengboy/api-boot/wiki/ApiBoot-Quartz)
- [ApiBoot DataSource Switch](https://github.com/hengboy/api-boot/wiki/ApiBoot-DataSource-Switch)
- [ApiBoot Resource Load](https://github.com/hengboy/api-boot/wiki/ApiBoot-Resource-Load)

### 微信扫码加入恒宇少年队伍

![微信公众号](http://image.yuqiyu.com/qrcode.jpg)
关注微信公众号，回复`加群`，获取交流群群号。


